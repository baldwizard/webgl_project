//global
//var xyz_array=new Array();
//var whole_file_read=false;
//class

function globalArray(rot_array)
{
	this.xyz_array=rot_array;
	this.whole_file_read=false;
	this.current_index=0;

	this.incrementIndex=function()
	{
		//if(!this.current_index)
		if(this.xyz_array.length-1>this.current_index)
		this.current_index+=1;
		else
		this.current_index=0;
	};
	this.getCurrentPoint=function()
	{
		//if(!this.xyz_array&&!this.current_index)
		return this.xyz_array[this.current_index];
		//else return null;
	};
}


//class
function point3d(x,y,z)
{
	this.x=parseFloat(x);
	this.y=parseFloat(y);
	this.z=parseFloat(z);

	
	this.getCoord= function()
	{
		return 'x,y,z('+this.x+','+this.y+','+this.z+')';
	}
}




function selectFile(_event)
{
	if(_event)
	{
		//alert(_event.target.files);
	var file=_event.target.files[0];

	readFile(file);
	//alert("File was read");
	}
	else
		{
			alert("Error file was not read");
		}
}


function readFile(file)
{
	//alert("1:"+file);

	if(file)
	{
		//alert("2:"+file);
		var output=new FileReader();

		//when file loads do this;
		output.onload= function(e)
		{
			//alert("3:"+e);
			var contents=e.target.result;

			//alert(file.name+" "+file.size+" "+contents); 
			parseContent(contents,file.name)
		};

		output.readAsText(file);
	}
	
}


function parseContent(contents,fname)
{
	var int_array=contents.split(",");
	var xyz_array=new Array();
	var i=0;
	var y=0;

	//alert("length:"+int_array.length+" modulo%3:"+int_array.length%3);
	if(!(int_array.length%3))
	{

			while(i<int_array.length)
			{
				xyz_array[y]=new point3d(int_array[i],int_array[i+1],int_array[i+2]);

				i+=3;
				y+=1;
			}
			
			globalArrayPoint3d=new globalArray(xyz_array);
			globalArrayPoint3d.whole_file_read=true;
	
			alert("File "+fname+" was parsed succesfully:"+globalArrayPoint3d.xyz_array.length+" rows read");
	}
	else 
	{
		alert("File has wrong formating should be (x,y,z)")
	}

}


/*function calcDelta()
{
	var currentTime = (new Date).getTime();
  	
  	if (lastSquareUpdateTime) {
  		var delta = currentTime - lastSquareUpdateTime;
  	
  		squareRotation += (30 * delta) / 1000.0;
  	}
  	
  	lastSquareUpdateTime = currentTime;
}*/

function addSpeed()
{
	if(speed<0)
	speed+=1;

	document.getElementById('currentSpeed').value=speed;
}

function decreaseSpeed()
{
	if(speed>-30)
	speed-=1;

	document.getElementById('currentSpeed').value=speed;
}

function reset()
{
	globalArrayPoint3d=null;
}

