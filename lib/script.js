	
	//globals
	//webGL
	var FIRST_TIME=true;

	var renderer;
	var camera;
	var scene;
	var light;

	//global class array for point 3d objects

	var globalArrayPoint3d;
	//var lastSquareUpdateTime=null;
	var speed=0;
	var skipFrame=0;
	//functions
	function init_renderer()
	{
		//renderer
		var renderer=new THREE.WebGLRenderer();

		//renderer init
		//canvas size
		renderer.setSize( 800, 600 );
        //renderer element gets added to dom in body
        document.body.appendChild( renderer.domElement );
	    return renderer;
	}

	function init_scene()
	{
		//scene
		var scene= new THREE.Scene();
		return scene;
	}

	function init_camera()
	{
		//camera
		var camera = new THREE.PerspectiveCamera(
            35,             // Field of view
            800 / 600,      // Aspect ratio
            0.1,            // Near plane
            10000           // Far plane
        );

		//camera position
         camera.position.set( -15, 10, 10 );
         //probably directing camera at the face of the scene
         camera.lookAt( scene.position ); 
		return camera;
	}

	function create_light (color,x,y,z) {		
        var light = new THREE.PointLight( ((!color)?0xFFFF00:color) );
        var default_pos=10;
        light.position.set( ((!x)?default_pos:x),((!y)?default_pos:y), ((!z)?default_pos:z));
        return light;
	}

	function create_cube()
	{
		var geo_cube = new THREE.CylinderGeometry(0, 0.5,5, 4,false );
        var material = new THREE.MeshLambertMaterial( { color: 0xFF0000 } );
        var cube = new THREE.Mesh( geo_cube, material );
        return cube;
	}

	var render= function()
 	{
		
		if(globalArrayPoint3d!=null && globalArrayPoint3d.whole_file_read)
 		{

 			if(skipFrame==0)
 			{
 			var tempPoint=globalArrayPoint3d.getCurrentPoint();
 			//alert(tempPoint.x +" "+ tempPoint.y+" "+tempPoint.z);
 			cube.rotation.x+=tempPoint.x;
 			cube.rotation.y+=tempPoint.y;
 			cube.rotation.z+=tempPoint.z;
 			globalArrayPoint3d.incrementIndex();

 			//slow faster
 			skipFrame=speed;
 			//alert(globalArrayPoint3d.current_index);
 			}
 			else 
 				{
 					skipFrame+=1;
 				}
 		
 		/*else
 		{
 			cube.rotation.x+=0.10;
 			cube.rotation.y+=0.1;
 			cube.rotation.z+=0.1;
 		}*/
 		renderer.render(scene,camera);
 		}
 		//calcDelta();
 		requestAnimationFrame(render);
 		//.rotation.y+=0.1;
 		//cube.rotation.z-=0.1;
 		//
	}

		//rendering loop
	
		if(FIRST_TIME)
  		{
  		renderer =init_renderer();
		scene    =init_scene();
		camera   =init_camera();
		
		// Check for the various File API support.
		if (window.File && window.FileReader && window.FileList && window.Blob) {
		  // Great success! All the File APIs are supported.
			document.getElementById('file_reader').addEventListener('change',selectFile,false);
		} else {
		  alert('The File APIs are not fully supported in this browser.');
		}
		
  		FIRST_TIME=false;
  		}

 		//main function and loop
		

		var cube=create_cube();
		
		scene.add( create_light(null,10,10,10));
		scene.add(create_light(null,-10,-10,-10));
		scene.add(cube);

		render();
	

 	//main();